import * as mongoose from 'mongoose';

const carSchema = new mongoose.Schema({
   
nDoors      : Number ,
   nPassengers : Number ,
   airConditioner : Boolean ,
   consumption    : String ,
   brand : String ,
   model : String ,
   photo : String ,
   suppliers : [] 
});

const Car = mongoose.model('Car', carSchema);

export default Car;
